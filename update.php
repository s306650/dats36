  <?php include 'header.php';?>
  <?php include 'dbconn.php';?>
  <?php
  $id = $_POST["id"];
  $name = $_POST["name"];
  $email = $_POST["email"];
  $program = $_POST["program"];
  $status = "ok";

  if (isset($_POST['Delete'])) {
    $status = "delete";
  } else if (isset($_POST['Update'])) {
    if (($id=="") || ($name=="") || ($email=="") || ($program=="")) {
      echo "<h3 class='error'>No empty values!!!</h3>";
      $status = "error";
    } else {
      $sql1 = "UPDATE students SET Name='$name' WHERE StudentID='$id'";
      $sql2 = "UPDATE students SET Email='$email' WHERE StudentID='$id'";
      $sql3 = "UPDATE program SET StudyProgram='$program' WHERE StudentID='$id'";
      if (($dbconn->query($sql1))&&($dbconn->query($sql2))&&($dbconn->query($sql3))) {
        echo "<h3 class='ok'>$name was sucsessfully updated!</h3>";
      } else {
        echo "<h3 class='error'>UPDATE attempt failed!</h3>";
        $status = "error";
      }
    }
    $dbconn->close();
  } else if (isset($_POST['Create'])) {
    if (($id=="") || ($name=="") || ($email=="") || ($program=="")) {
      echo "<h3 class='error'>No empty values!!!</h3>";
      $status = "error";
    } else {
      if (preg_match('/^\d{6}$/', $id)) {
        $sql = "SELECT StudentID FROM students WHERE (StudentID=$id)";
        $result = $dbconn->query($sql);
        $row = $result->fetch_assoc();
        $result->close();
        if (($row['StudentID'])!=($id)) {
          $sql1 = "INSERT INTO students (StudentID, Name, Email) VALUES ($id, '$name', '$email')";
          $sql2 = "INSERT INTO program (StudentID, StudyProgram) VALUES ($id, '$program')";
          if (($dbconn->query($sql1))&&($dbconn->query($sql2))) {
            echo "<h3 class='ok'>$name was sucsessfully created!</h3>";
          } else {
            echo "<h3 class='error'>INSERT attempt failed!</h3>";
            $status = "error";
          }
        } else {
          echo "<h3 class='error'>You can not use same StudentID twice!</h3>";
          $status = "error";
        }
      } else {
        echo "<h3 class='error'>StudentID must be six digits!</h3>";
        $status = "error";
      }
      $dbconn->close();
    }
  }
  ?>
  <br />
  <?php
  if (($status)==('ok')) {
    echo "<form action='index.php' method='POST'>";
    echo "<button class='button_ok' type='submit' value='OK'>OK</button>";
  } else if (($status)==('error')) {
    echo "<form action='add.php' method='POST'>";
    echo "<input type='hidden' name='id' value='$id'>";
    echo "<input type='hidden' name='name' value='$name'>";
    echo "<input type='hidden' name='email' value='$email'>";
    echo "<input type='hidden' name='program' value='$program'>";
    echo "<button class='button_ok' type='submit' value='Back'>Go back</button>";
  } else if (($status)==('delete')) {
    echo "<h3 class='error'>Are you sure you want to delete $name?</h3><br /><br /><br />";
    echo "<table border='0'>";
    echo "<tr><td><a class='button' href='edit.php?id=$id'>No</a></td>";
    echo "<td><a class='button' href='delete.php?id=$id&&name=$name'>Yes</a></td></tr>";
    echo "</table>";
  }
  ?>
  </form>
  <?php include 'footer.php';?>
