	<?php include 'header.php';?>
	<?php include 'dbconn.php';?>
	<p>Enter the values you want to update.<br />
	Click cancel to go back to the startpage.<br />
	Click update to save all changes.<br />
	Click delete to remove user from the database.</p><br />
	<form action="update.php" method="POST">
		<?php
		$id = $_GET['id'];
		$sql = "SELECT s.StudentID, s.Name, s.Email, p.StudyProgram
			FROM students AS s, program AS p
			WHERE s.StudentID=p.StudentID AND s.StudentID=$id";
		$result = $dbconn->query($sql);

		echo "<table border='0'>";
		echo "<tr><th>Field</th><th>Value</th></tr>";

		while ($row = $result->fetch_assoc()) {
			echo "<tr><td>StudentID:</td><td><input type='int' name='id' readonly='readonly' value='{$row['StudentID']}'></td></tr>
			<tr><td>Name:</td><td><input type='text' name='name' value='{$row['Name']}'></td></tr>
			<tr><td>E-mail:</td><td><input type='text' name='email' value='{$row['Email']}'></td></tr>
			<tr><td>Program:</td><td><input type='text' name='program' value='{$row['StudyProgram']}'></td></tr>";
		}

		echo "</table>";
		$result->close();
		$dbconn->close();
		?>
		<br />
		<table border='0'>
			<tr><td><button class="button" type="submit" name="Delete" value="Delete">Delete</button></td>
			<td><button class="button" type="submit" name="Update" value="Update">Update</button></td>
	</form>
	<form action="index.php" method="POST">
		<td><button class="button" type="submit" value="Cancel">Cancel</button></td></tr>
	</form>
	</table>
	<?php include 'footer.php';?>
