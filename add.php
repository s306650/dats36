  <?php include 'header.php';?>
  <?php include 'dbconn.php';?>
  <p>Enter the values you want to update.<br />
  Click cancel to go back to the startpage.<br />
  Click reset to remove all values.<br />
  Click create to add user to the database.</p><br />
  <form action="update.php" method="POST">
    <table border='0'>
      <tr><th>Field</th><th>Value</th></tr>
      <?php
      if (empty($_POST['id'])) {
        echo "<tr><td>StudentID:</td><td><input type='int' name='id' value=''></td></tr>";
        echo "<tr><td>Name:</td><td><input type='text' name='name' value=''></td></tr>";
        echo "<tr><td>E-mail:</td><td><input type='text' name='email' value=''></td></tr>";
        echo "<tr><td>Program:</td><td><input type='text' name='program' value=''></td></tr>";
      } else {
        $id = $_POST['id'];
        $name = $_POST['name'];
        $email = $_POST['email'];
        $program = $_POST['program'];
        echo "<tr><td>StudentID:</td><td><input type='int' name='id' value='$id'></td></tr>";
        echo "<tr><td>Name:</td><td><input type='text' name='name' value='$name'></td></tr>";
        echo "<tr><td>E-mail:</td><td><input type='text' name='email' value='$email'></td></tr>";
        echo "<tr><td>Program:</td><td><input type='text' name='program' value='$program'></td></tr>";
      }
      ?>
    </table>
    <br />
    <table border='0'>
      <tr><td><button class="button" type="submit" name="Create" value="Create">Create</button></td>
      <td><button class="button" type="reset" value="Reset">Reset</button></td>
  </form>
  <form action="index.php" method="POST">
    <td><button class="button" type="submit" value="Cancel">Cancel</button></td></tr>
  </form>
  </table>
  <?php include 'footer.php';?>
