	<?php include 'dbconn.php';?>
	<p>Click on the name to edit a student.<br />
	Add new students with the button below.</p><br />
	<?php
	$sql = "SELECT s.StudentID, s.Name, p.StudyProgram
		FROM students AS s, program AS p
		WHERE s.StudentID=p.StudentID
		ORDER BY s.Name";
	$result = $dbconn->query($sql);

	echo "<table border='0'>";
	echo "<tr><th>ID</th><th>Name</th><th>Study Program</th></tr>";

	while ($row = $result->fetch_assoc()) {
		echo "<tr><td>s{$row['StudentID']}</td>
		<td><a href='edit.php?id={$row['StudentID']}'>{$row['Name']}</a></td>
		<td>{$row['StudyProgram']}</td></tr>";
	}

	echo "</table><br />";
	$result->close();
	$dbconn->close();
	?>
	<table border='0'>
		<form action="index.php" method="POST">
			<tr><td><button class="button" type="submit" value="Reload">Reload page</button></td>
		</form>
		<form action="add.php" method="POST">
			<td><button class="button" type="submit" value="Add">Add student</button></td></tr>
		</form>
	</table>
